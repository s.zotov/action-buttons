const fibery = context.getService('fibery');

const TOP_LIMIT = 3;


async function getReferences(interviewId) {
    return await fibery.executeSingleCommand({
        "command": "fibery.entity/query",
        "args": {
            "query": {
                "q/from": "Collaboration~Documents/Reference",
                "q/select": {
                    "id": "fibery/id",
                    "toEntityId": "Collaboration~Documents/ToEntityId",
                    "toEntityType": {
                        "id": ["Collaboration~Documents/ToEntityType", "fibery/id"],
                        "name": ["Collaboration~Documents/ToEntityType", "fibery/name"]
                    },
                    "paragraphSecret": "Collaboration~Documents/ParagraphSecret"
                },
                "q/where": ["=", ["Collaboration~Documents/FromEntityId"], "$id"],
                "q/limit": "q/no-limit"
            },
            "params": { "$id": interviewId }
        }
    });
}

async function getTopRecurringObservations(study, topLimit) {
    const references = await Promise.all(study["Interviews"].map(async (interview) =>
        await getReferences(interview.id)));

    const entitiesReferencedObj = [].concat(...references).reduce((acc, reference) => {
        if (!acc[reference.toEntityId]) {
            acc[reference.toEntityId] = {
                type: reference.toEntityType,
                count: 0,
                paragraphSecrets: []
            }
        }

        acc[reference.toEntityId].count++;
        acc[reference.toEntityId].paragraphSecrets.push(reference.paragraphSecret);

        return acc;
    }, {});

    const entitiesReferenced = Object.keys(entitiesReferencedObj).map(key =>
        Object.assign({ id: key }, entitiesReferencedObj[key]));
    entitiesReferenced.sort((e1, e2) => e2.count - e1.count);
    const entitiesFeatured = entitiesReferenced.slice(0, topLimit);

    return {
        study,
        topRecurringObservations: entitiesFeatured
    }
}


function buildResultsJSON(entities) {
    const emojis = ["🥇", "🥈", "🥉"];

    const entitiesJSON = entities.map((e, i) => {
        return [
            {
                type: "heading",
                attrs: {
                    level: 3
                },
                content: [{
                    text: `${emojis[i] || "🦻"} – ${e.count} mention${e.count > 1 ? "s" : ""}`,
                    type: "text"
                }]
            },
            {
                type: "paragraph",
                content: [{
                    type: 'entity',
                    attrs: {
                        id: e.id,
                        typeId: e.type.id
                    }
                }]
            }/*,
            {
                type: "code_block",
                content: [{
                    text: "Quote",
                    type: "text"
                }]
            }*/
        ];
    });
    const body = [].concat(...entitiesJSON);

    const header = {
        type: "heading",
        attrs: {
            level: 2
        },
        content: [{
            text: "Top recurring observations",
            type: "text"
        }]
    }

    const doc = {
        doc: {
            type: 'doc',
            content: [header, ...body]
        }
    };

    return JSON.stringify(doc);
}

const studies = (await Promise.all(args.currentEntities.map(async (e) => {
    const s = await fibery.getEntityById(e.type, e.id, ["Interviews"]);
    return Object.assign({}, s, e);
}))).filter(s => s["Interviews"].length > 0);

if (!studies.length) {
    throw new Error("Please add Interviews with notes to generate results")
}

const studiesObservations = (await Promise.all(studies.map(async (s) => await getTopRecurringObservations(s, TOP_LIMIT))))
    .filter(s => s.topRecurringObservations.length > 0);

if (!studiesObservations.length) {
    throw new Error("Please link Interview notes to Insights to generate results – check out the Read.me")
}

for (const studyObservation of studiesObservations) {
    const resultsJSON = buildResultsJSON(studyObservation.topRecurringObservations);
    const secret = studyObservation.study["Results"].secret;
    const currentMD = await fibery.getDocumentContent(secret, "md");
    const method = currentMD && currentMD.length > 0 ? "appendDocumentContent" : "setDocumentContent";
    await fibery[method](secret, resultsJSON, "json");
}

return "The results are coming...";