const fibery = context.getService('fibery');

const PLANNED_TYPE_NAME = 'Project';

for (const stage of args.currentEntities) {
    await Promise.all([
        await fibery.setState(stage.type, stage.id, "In Progress"),
        await fibery.updateEntity(stage.type, stage.id, {
            "Dates": {
                "start": (new Date()).toISOString(),
                "end": stage["Dates"].end
            }
        })
    ]);

    const stageTemplate = await fibery.getEntityById("Stage Template", stage["Stage Template"].id, ["Task Templates"]);
    const taskTemplateIds = stageTemplate["Task Templates"].map(tt => tt.id);
    const taskTemplates = await fibery.getEntitiesByIds("Task Template", taskTemplateIds, ["Name", "fibery/rank"]);

    for (const taskTemplate of taskTemplates) {
        // when there are multiple "Task" Types in different Apps, use `${App}/Task` syntax
        await fibery.createEntity('Task', {
            "name": taskTemplate["Name"],
            "fibery/rank": taskTemplate["fibery/rank"],
            "Stage": stage.id
        });
    }

    await fibery.updateEntity(PLANNED_TYPE_NAME, stage[PLANNED_TYPE_NAME].id, {
        "Current Stage": stage["Stage Template"].id
    });
}