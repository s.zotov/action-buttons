const fibery = context.getService('fibery');

const APP_NAME = "Process";
const TEMPLATE_APP_NAME = "Process Template";
const TYPE_NAME = "Project";
const STAGE_DEFAULT_DURATION = 7;

const stageTemplates = await fibery.executeSingleCommand({
    "command": "fibery.entity/query",
    "args": {
        "query": {
            "q/from": `${TEMPLATE_APP_NAME}/Stage Template`,
            "q/select": {
                "id": "fibery/id",
                "rank": "fibery/rank",
                "name": `${TEMPLATE_APP_NAME}/name`,
                "duration": `${TEMPLATE_APP_NAME}/Default Duration`
            },
            "q/order-by": [
                [["fibery/rank"], "q/asc"]
            ],
            "q/limit": "q/no-limit"
        }
    }
});


const today = new Date();
const addDays = (date, days) => {
    let result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}


for (const entity of args.currentEntities) {
    let offsetDays = 0;
    for (const stage of stageTemplates.slice(1, -1)) {
        const startDate = addDays(today, offsetDays);
        const duration = stage.duration || STAGE_DEFAULT_DURATION;
        const endDate = addDays(startDate, duration);
        offsetDays += duration;
        console.log(stage.name, startDate, endDate);

        await fibery.createEntity(`${APP_NAME}/Stage`, {
            "name": stage.name,
            "Stage Template": stage.id,
            [TYPE_NAME]: entity.id,
            "fibery/rank": stage.rank,
            "Dates": {
                "start": startDate.toISOString(),
                "end": endDate.toISOString()
            }
        });
    }

    if (!entity["Current Stage"].id) {
        await fibery.updateEntity(entity.type, entity.id, {
            "Current Stage": stageTemplates[0].id
        })
    }
}