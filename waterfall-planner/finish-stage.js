const fibery = context.getService('fibery');

const today = new Date();
const addDays = (date, days) => {
    let result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
};

for (const stage of args.currentEntities) {
    if (stage["# of Tasks"] && stage["# of Tasks"] - stage["# of Done Tasks"] !== 0) {
        throw new Error("some tasks in this stage are not done 🙈");
    }

    await Promise.all([
        await fibery.setStateToFinal(stage.type, stage.id),
        await fibery.updateEntity(stage.type, stage.id, {
            "Dates": {
                "start": stage["Dates"].start,
                "end": addDays(today, 1).toISOString()
            }
        })
    ]);

}