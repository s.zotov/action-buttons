const fibery = context.getService('fibery');

const FIELD_NAME = 'Upvoters';

await Promise.all(
    args.currentEntities.map(async e => {
        const entity = await fibery.getEntityById(e.type, e.id, [FIELD_NAME]);
        const userId = args.currentUser.id;
        const usersCollection = entity[FIELD_NAME].map(({ id }) => id);
        const methodName = usersCollection.includes(userId) ? 'removeCollectionItem' : 'addCollectionItem';
        return fibery[methodName](e.type, e.id, FIELD_NAME, args.currentUser.id);
    })
);