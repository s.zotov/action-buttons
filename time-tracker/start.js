const api = context.getService("fibery");

for (const entity of args.currentEntities) {
    await api.updateEntity(entity.type, entity.id, {"Timer Started": (new Date()).toISOString()});
}