const api = context.getService("fibery");
await Promise.all(args.currentEntities.map((e) => api.assignUser(e.type, e.id, args.currentUser.id)));
